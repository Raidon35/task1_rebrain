provider "gitlab"  {
	token = "${var.token-gitlab}"
}
resource "gitlab_project" "sample_project" {
	name = "Task1_Rebrain"
	visibility_level = "public"
}

resource "gitlab_deploy_key" "sample_deploy_key" {
    project = "${gitlab_project.sample_project.id}"
	title = "test key"
	key = "${var.gitlab-key}"
	can_push = "True"

}
